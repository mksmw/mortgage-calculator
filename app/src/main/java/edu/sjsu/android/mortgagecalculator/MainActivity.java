package edu.sjsu.android.mortgagecalculator;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.renderscript.Float4;
import android.text.TextUtils;
import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Button;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    private EditText amount;
    public TextView interest;
    private SeekBar interestBar;
    public TextView output;
    public CheckBox taxInsurance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        amount = (EditText) findViewById(R.id.amountInput);
        interest = findViewById(R.id.interestNumber);
        interestBar = findViewById(R.id.interestRateBar);
        taxInsurance = findViewById(R.id.taxCheckBox);
        output = findViewById(R.id.outputText);

        interestBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float value = (float) (i * 0.1);
                interest.setText("" + value);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void onClick(View view) {
        int interestValue = interestBar.getProgress();
        float interestSelected = (float) (interestValue * 0.1);
        float newInterest = (interestSelected / 1200);
        int amountInput = Integer.parseInt(amount.getText().toString());
        float tax = (float) (amountInput * (0.1 / 100));
        float amountOutput;

        switch (view.getId()) {
            // Case when calculate button is pressed
            case R.id.calculateButton:
                RadioButton fifteenButton = (RadioButton) findViewById(R.id.radioButton15);
                RadioButton twentyButton = (RadioButton) findViewById(R.id.radioButton20);
                RadioButton thirtyButton = (RadioButton) findViewById(R.id.radioButton30);

                // Case for 15 months
                if (fifteenButton.isChecked()) {

                    if (amountInput == 0.0) {
                        Toast.makeText(this, "Please enter a valid number",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    // Two int variables for negative and positive (months * 12)
                    int months = -180;
                    int term = 180;

                    // Case for tax button not checked and interest rate > 0
                    amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months)))));
                    output.setText(String.valueOf(amountOutput));

                    // Case for tax button checked and interest rate > 0
                    if (taxInsurance.isChecked()) {
                        amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months))))) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is checked and interest rate is = 0.0
                    else if (taxInsurance.isChecked() && interestValue == 0){
                        amountOutput = (float) (amountInput / term) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is not checked and interest rate is 0.0
                    else if (interestSelected == 0.0) {
                        amountOutput = (float) (amountInput / term);
                        output.setText(String.valueOf(amountOutput));
                    }
                }

                // Case for 20 months
                if (twentyButton.isChecked()) {
                    if (amount.getText().length() == 0) {
                        Toast.makeText(this, "Please enter a valid number",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    // Two int variables for negative and positive (months * 12)
                    int months = -240;
                    int term = 240;

                    // Case for tax button not checked and interest rate > 0
                    amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months)))));
                    output.setText(String.valueOf(amountOutput));

                    // Case for tax button checked and interest rate > 0
                    if (taxInsurance.isChecked()) {
                        amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months))))) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is checked and interest rate is = 0.0
                    else if (taxInsurance.isChecked() && interestValue == 0){
                        amountOutput = (float) (amountInput / term) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is not checked and interest rate is 0.0
                    else if (interestSelected == 0.0) {
                        amountOutput = (float) (amountInput / term);
                        output.setText(String.valueOf(amountOutput));
                    }
                }

                // Case for 30 months
                if (thirtyButton.isChecked()) {
                    if (amount.getText().length() == 0) {
                        Toast.makeText(this, "Please enter a valid number",
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    // Two int variables for negative and positive (months * 12)
                    int months = -360;
                    int term = 360;

                    // Case for tax button not checked and interest rate > 0
                    amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months)))));
                    output.setText(String.valueOf(amountOutput));

                    // Case for tax button checked and interest rate > 0
                    if (taxInsurance.isChecked()) {
                        amountOutput = (float) (amountInput * (newInterest / (1 - (Math.pow(1 + newInterest, months))))) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is checked and interest rate is = 0.0
                    else if (taxInsurance.isChecked() && interestValue == 0){
                        amountOutput = (float) (amountInput / term) + tax;
                        output.setText(String.valueOf(amountOutput));
                    }
                    // Case for tax button is not checked and interest rate is 0.0
                    else if (interestSelected == 0.0) {
                        amountOutput = (float) (amountInput / term);
                        output.setText(String.valueOf(amountOutput));
                    }
                }
        }
    }
}
